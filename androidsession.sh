DOGE="░░░░░░░░░█▐▓█░░░░░░░░█▀▄▓▌█░░░░░░"
DOGE+="\n░░░░░░░░░█▐▓▓████▄▄▄█▀▄▓▓▓▌█░░░░░"
DOGE+="\n░░░░░░░▄██▐▓▓▓▄▄▄▄▀▀▀▄▓▓▓▓▓▌█░░░░"
DOGE+="\n░░░░░▄█▀▀▄▓█▓▓▓▓▓▓▓▓▓▓▓▓▀░▓▌█░░░░"
DOGE+="\n░░░░█▀▄▓▓▓███▓▓▓███▓▓▓▄░░▄▓▐█▌░░░"
DOGE+="\n░░░█▌▓▓▓▀▀▓▓▓▓███▓▓▓▓▓▓▓▄▀▓▓▐█░░░"
DOGE+="\n░░▐█▐██▐░▄▓▓▓▓▓▀▄░▀▓▓▓▓▓▓▓▓▓▌█▌░░"
DOGE+="\n░░█▌███▓▓▓▓▓▓▓▓▐░░▄▓▓███▓▓▓▄▀▐█░░"
DOGE+="\n░▐█▐█▓▀░░▀▓▓▓▓▓▓▓▓▓██████▓▓▓▓▐█░░"
DOGE+="\n░▐▌▓▄▌▀░▀░▐▀█▄▓▓██████████▓▓▓▌█▌░"
DOGE+="\n░▐▌▓▓▓▄▄▀▀▓▓▓▀▓▓▓▓▓▓▓▓█▓█▓█▓▓▌█▌░"
DOGE+="\n░░█▐▓▓▓▓▓▓▄▄▄▓▓▓▓▓▓█▓█▓█▓█▓▓▓▐█░░"
DOGE+="\n░░░█▐▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▐█░░░"
DOGE+="\n🐶  𝘿𝙊𝙂𝙀  𝙐𝙎𝙀𝙍𝘽𝙊𝙏  𝙎𝙀𝙏𝙐𝙋  🐾"
MESAJ="\n📣 TELEGRAM: @DogeUserBot"
MESAJ+="\n "
MESAJ+="\n🧩 PLUGIN: @DogePlugin"
MESAJ+="\n "
MESAJ+="\n🌀 İŞLEM DEVAM EDERKEN UYGULAMADAN AYRILMAYIN!"
MESAJ+="\n "
MESAJ+="\n🌀 DO NOT EXIT THE APP WHILE THE PROCESS IS PROCESSING!"
YARDIM="\n📌 KURULUM %50, %70 VE %75'TE DURAKLADIĞINDA Y YAZIP ENTERLAYIN!"
YARDIM+="\n "
YARDIM+="\n📌 WRITE Y AND ENTER WHEN THE INSTALLATION IS PAUSED AT 50%, 70% AND 75%!"
PACKAGEX="\n⏳ PAKETLERİ GÜNCELLİYORUM..."
PACKAGEX+="\n "
PACKAGEX+="\n⏳ I'M UPDATING PACKAGES..."
PYTHOX="\n⌛ PYTHON KURUYORUM..."
PYTHOX+="\n "
PYTHOX+="\n⌛ I'M INSTALLING PYTHON..."
GIX="\n⌛ GIT KURUYORUM..."
GIX+="\n "
GIX+="\n⌛ I'M INSTALLING GIT..."
TELETHOX="\n⌛ TELETHON KURUYORUM..."
TELETHOX+="\n "
TELETHOX+="\n⌛ I'M INSTALLING TELETHON..."
SESSIOX="\n⌛ 🐶 DOGE OTURUMCUSUNU İNDİRİYORUM..."
SESSIOX+="\n "
SESSIOX+="\n⌛ 🐶 I'M DOWNLOADING DOGE SESSIONER..."
REQUIREX="\n⌛ GEREKSİNİMLERİ KURUYORUM..."
REQUIREX+="\n "
REQUIREX+="\n⌛ I'M INSTALLING REQUIREMENTS..."
BOSLUK="\n "
echo -e $DOGE
echo -e $BOSLUK
echo -e $YARDIM
echo -e $BOSLUK
sleep 1
echo -e $PACKAGEX
echo -e $BOSLUK
rm -rf dogesession
pkg update -y
clear
echo -e $DOGE
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e $PYTHOX
echo -e $BOSLUK
pkg install python -y
pip install --upgrade pip
clear
echo -e $DOGE
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e $GIX
echo -e $BOSLUK
pkg install git -y
clear
echo -e $DOGE
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e $TELETHOX
echo -e $BOSLUK
pip install telethon
clear
echo -e $DOGE
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e $SESSIOX
echo -e $BOSLUK
git clone -b SESSION https://TeleDoge@bitbucket.org/dog-e/doge
clear
echo -e $DOGE
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e $REQUIREX
echo -e $BOSLUK
cd doge
pip install wheel
pip install -r requirements.txt
clear
python -m dogesession